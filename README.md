# Introduction

The Commerce SimplePay by OTP module provides an OTP SimplePay payment
 gateway option

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_otpsp

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_otpsp


## Requirements

 * PHP 7.1
 * [OTP SimplePay client](https://packagist.org/packages/cheppers/otpsp-client) (`cheppers/otpsp-client`)
 * [Drupal Commerce](https://www.drupal.org/project/commerce) (`drupal/commerce`)


## Recommended modules

 * [Commerce Shipping](https://www.drupal.org/project/commerce_shipping) (`drupal/commerce_shipping`)


# Installation

 `composer run drupal/commerce_otpsp`


## Configuration

1. Add a new payment gateway. \
   _Commerce » Configuration » Payment » Payment gateways_ \
   _/admin/commerce/config/payment-gateways/add_
1. Choice _SimplePay (Redirect to SimplePay)_ as plugin.
1. Merchant ID and Secret key \
   You can find these on your SimplePay Dashboard \
   _Partner » Select your company » Account
   1. Go to https://sandbox.simplepay.hu/admin/index
   1. Click on your company name
   1. You can find the _Merchant id_ at the top of the page.
   1. Activate the _Technical_ tab (Next to the Business tab)
   1. There you can find your _SECRET_KEY_
   1. Go back to your Drupal Commerce site
1. Save your new payment gateway
1. Configure the IPN URL on SimplePay Dashboard \
   _Partner » Select your company » Account » Technical_
   1. Go to https://sandbox.simplepay.hu/admin/index
   1. Click on your company name
   1. Activate the _Technical_ tab
   1. Fill the following URL into the _IPN URL_ field in the _Base data_ section \
      (replace the placeholders with real values) \
      https://{your_domain)/payment/notify/{payment_gateway_machine_name}
