<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp\PluginForm;

use Cheppers\OtpspClient\OtpSimplePayClient;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_otpsp\PaymentRequestBuilderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SimplePayRedirectOffSitePaymentForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Cheppers\OtpspClient\OtpSimplePayClient
   */
  protected $client;

  /**
   * @var \Drupal\commerce_otpsp\PaymentRequestBuilderInterface
   */
  protected $paymentRequestBuilder;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_otpsp.client'),
      $container->get('commerce_otpsp.payment_request_builder')
    );
  }

  public function __construct(
    OtpSimplePayClient $client,
    PaymentRequestBuilderInterface $paymentRequestBuilder
  ) {
    $this->client = $client;
    $this->paymentRequestBuilder = $paymentRequestBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $payment->setRemoteState('INIT');
    $payment->save();

    $paymentRequest = $this->paymentRequestBuilder->getPaymentRequest($payment);

    $paymentResponse = $this->client->startPayment($paymentRequest);
    if ($paymentResponse->errorCodes !== []) {
      throw new PaymentGatewayException('Payment init failed', $paymentResponse->errorCodes[0]);
    }

    $payment->setRemoteId((string) $paymentResponse->transactionId)->save();

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $paymentResponse->paymentUrl,
      $paymentRequest->jsonSerialize(),
      static::REDIRECT_POST
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = self::REDIRECT_GET) {
    if ($redirect_method == self::REDIRECT_POST) {
      $form['#attached']['library'][] = 'commerce_payment/offsite_redirect';
      $form['#process'][] = [parent::class, 'processRedirectForm'];
      $form['#redirect_url'] = $redirect_url;

      foreach ($data as $key => $value) {
        // Prevent array to string conversion by adding multi value form fields.
        if (is_array($value) && array_key_first($value) === 0 && !is_scalar($value[0])) {

          foreach ($value as $delta => $item) {
            $form[$key][$delta] = [
              '#type' => 'hidden',
              '#value' => $item,
              '#parents' => [$key],
            ];
          }

          continue;
        }

        // Original code from parent method.
        $form[$key] = [
          '#type' => 'hidden',
          '#value' => $value,
          '#parents' => [$key],
        ];
      }
      $form['commerce_message'] = [
        '#markup' => '<div class="checkout-help">' . t('Please wait while you are redirected to the payment server. If nothing happens within 10 seconds, please click on the button below.') . '</div>',
        '#weight' => -10,
      ];
    }
    else {
      $redirect_url = Url::fromUri($redirect_url, ['absolute' => TRUE, 'query' => $data])->toString();
      throw new NeedsRedirectException($redirect_url);
    }

    return $form;
  }

}
