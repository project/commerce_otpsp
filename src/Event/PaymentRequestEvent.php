<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp\Event;

use Symfony\Contracts\EventDispatcher\Event;

class PaymentRequestEvent extends Event {

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  public $payment;

  /**
   * @var \Cheppers\OtpspClient\DataType\PaymentRequest
   */
  public $paymentRequest;

}
