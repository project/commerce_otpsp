<?php

namespace Drupal\commerce_otpsp;

use Cheppers\OtpspClient\DataType\PaymentRequest;
use Drupal\commerce_payment\Entity\PaymentInterface;

interface PaymentRequestBuilderInterface {

  const EVENT_ALTER = 'commerce_otpsp.payment_request.alter';

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The given payment has to belong to an order.
   */
  public function getPaymentRequest(PaymentInterface $payment): PaymentRequest;

}
