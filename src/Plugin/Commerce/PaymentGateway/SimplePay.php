<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp\Plugin\Commerce\PaymentGateway;

use Cheppers\OtpspClient\OtpSimplePayClientInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides SimplePay payment gateway.
 *
 * @\Drupal\commerce_payment\Annotation\CommercePaymentGateway(
 *   id = "commerce_otpsp_redirect",
 *   label = @Translation("Redirection to SimplePay by OTP Mobile"),
 *   display_label = @Translation("SimplePay by OTP Mobile"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_otpsp\PluginForm\SimplePayRedirectOffSitePaymentForm",
 *   },
 *   credit_card_types = {
 *     "mastercard",
 *     "maestro",
 *     "visa",
 *     "amex",
 *   },
 * )
 */
class SimplePay extends OffsitePaymentGatewayBase {

  /**
   * @var \Cheppers\OtpspClient\OtpSimplePayClientInterface
   */
  protected $client;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Cheppers\OtpspClient\DataType\BackResponse
   */
  protected $backResponse;

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_otpsp.client'),
      $container->get('database')
    );
  }

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    OtpSimplePayClientInterface $client,
    Connection $database
  ) {
    $this->client = $client;
    $this->database = $database;
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );
    $this
      ->client
      ->setSecretKey($this->configuration['secret_key']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'secret_key' => '',
      'send_shipping' => FALSE,
      'timeout' => 10 * 60,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $gatewayId = $this->pluginDefinition['provider'];
    if ($gatewayId) {
      $form['ipn_url'] = [
        '#type' => 'textfield',
        '#disabled' => TRUE,
        '#title' => $this->t('IPN URL'),
        '#default_value' => Url::fromRoute(
          'commerce_payment.notify',
          ['commerce_payment_gateway' => $gatewayId],
          ['absolute' => TRUE]
        )->toString(),
        '#description' => $this->t('Value of this field may vary according to development environments.'),
      ];
    }

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('The merchant ID from your SimplePay account.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('The secret key of your SimplePay account.'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['send_shipping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send shipping'),
      '#description' => $this->t('Send checkout shipping info to SimplePay.'),
      '#default_value' => $this->configuration['send_shipping'],
      '#required' => FALSE,
      '#access' => $this->entityTypeManager->hasDefinition('commerce_shipment'),
    ];

    $form['timeout'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('The time limit for the customer to finish the payment process.'),
      '#default_value' => $this->configuration['timeout'],
      '#empty_option' => $this->t('- Select -'),
      '#empty_value' => '',
      '#options' => $this->getTimeoutOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['secret_key'] = $values['secret_key'];
    $this->configuration['send_shipping'] = $values['send_shipping'];
    $this->configuration['timeout'] = $values['timeout'];
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $bodyContent = $request->getContent();
    $signature = $request->headers->get('signature');

    if (
      !$bodyContent
      || !$signature
      || !$request->headers->has('Content-Type')
      || $request->headers->get('Content-Type') !== 'application/json'
    ) {
      // @todo Log - Check you Merchant ID and your secret key configured in
      // the Payment gateway instance.
      throw new PaymentGatewayException('Invalid instant payment notification.');
    }

    $ipn = $this
      ->client
      ->parseInstantPaymentNotificationMessage($signature, $bodyContent);

    $failedResponse = new Response(
      'IPN has not been processed',
      400
    );

    $ipnResponse = $this->client->getInstantPaymentNotificationSuccessParts($ipn);
    $successResponse = new Response(
      $ipnResponse['body'],
      $ipnResponse['statusCode'],
      $ipnResponse['headers']
    );

    $payment = $this->getPaymentByUuid($ipn->orderRef);

    if (!$payment) {
      // Payment with the given UUID is not exists any more.
      // We don't want to receive the same IPN again.
      //
      // The payment entity could be reproducible by fetching its details from
      // SimplePay through https://sandbox.simplepay.hu/payment/v2/query
      // See SimplePay documentation 3.11.
      // But we still can't figure out that which order the missing payment
      // belongs to.
      return $successResponse;
    }

    try {
      $payment
        ->setRemoteState($ipn->status)
        ->setState('completed')
        ->save();
    }
    catch (Exception $exception) {
      return $failedResponse;
    }

    return $successResponse;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $this->order = $order;
    $this->request = $request;
    $this->backResponse = $this->client->parseBackResponse($request->getUri());
    $this->payment = $this->getPaymentByUuid($this->backResponse->orderId);

    $this->onReturnUpdatePayment()->onReturnAddMessage();
  }

  /**
   * Add a success message to the user.
   *
   * @return $this
   */
  protected function onReturnAddMessage() {
    $this->messenger()->addMessage(
      $this->t('SimplePay transaction identifier: %transaction_id', [
        '%transaction_id' => $this->backResponse->transactionId,
      ]),
    );

    return $this;
  }

  protected function onReturnUpdatePayment() {
    if (!$this->payment) {
      // @todo Log entry.
      throw new PaymentGatewayException(
        'Payment with UUID ' . $this->backResponse->orderId . ' not found',
      );
    }

    if ($this->payment->getState()->getId() !== 'new') {
      // IPN is already arrived.
      return $this;
    }

    if ($this->backResponse->event !== 'SUCCESS') {
      $this->payment
        ->setRemoteState($this->backResponse->event)
        ->setState('authorization_voided')
        ->save();

      throw new PaymentGatewayException('Payment failed');
    }

    $this->payment
      ->setRemoteState('INFRAUD')
      ->setState('authorization')
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->order = $order;
    $this->request = $request;
    $this->backResponse = $this->client->parseBackResponse($request->getUri());
    $this->payment = $this->getPaymentByUuid($this->backResponse->orderId);

    $this
      ->onCancelUpdatePayment()
      ->onCancelAddMessage();
  }

  /**
   * @return $this
   */
  protected function onCancelUpdatePayment() {
    if (!$this->payment) {
      // @todo Log entry.
      return $this;
    }

    $this->payment
      ->setState('authorization_voided')
      ->setRemoteState($this->backResponse->event)
      ->save();

    return $this;
  }

  /**
   * @return $this
   */
  protected function onCancelAddMessage() {
     $this->messenger()->addMessage(
        $this->t('Transaction failed.<br /><br />SimplePay transaction ID: %transaction_id<br /><br />Please check the correctness of the data entered during the transaction. If you have entered all the data correctly, please contact your card issuing bank to investigate the reason for the refusal.', [
          '%transaction_id' => $this->backResponse->transactionId,
        ]),
        MessengerInterface::TYPE_WARNING,
     );

    return $this;
  }

  protected function getPaymentByUuid(string $uuid): ?Payment {
    /** @var \Drupal\commerce_payment\Entity\Payment[] $payments */
    $payments = $this
      ->entityTypeManager
      ->getStorage('commerce_payment')
      ->loadByProperties(['uuid' => $uuid]);

    return reset($payments) ?: NULL;
  }

  /**
   * @return string[]|
   */
  protected function getTimeoutOptions(): array {
    $minutes = [3, 5, 10, 15, 20, 30, 45, 60];
    $options = [];
    foreach ($minutes as $minute) {
      $options[$minute * 60] = $this->formatPlural($minute, '@count minute', '@count minutes');
    }

    return $options;
  }

}
