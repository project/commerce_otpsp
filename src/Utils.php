<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp;

class Utils {

  public static function transformAddressField(array $address): array {
    return [
      'name' => implode(
        ' ',
        array_filter([
          $address['family_name'] ?? '',
          $address['given_name'] ?? '',
        ])
      ),
      'country' => $address['country_code'] ?? '',
      'state' => $address['locality'] ?? '',
      'zip' => $address['postal_code'] ?? '',
      'city' => $address['locality'] ?? '',
      'address' => $address['address_line1'] ?? '',
      'address2' => $address['address_line2'] ?? '',
    ];
  }

}
