<?php

namespace Drupal\commerce_otpsp;

use Cheppers\OtpspClient\Checksum;
use Cheppers\OtpspClient\OtpSimplePayClient;
use Cheppers\OtpspClient\OtpSimplePayClientInterface;
use Drupal\Component\Datetime\TimeInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

class OtpspClientFactory {

  public function get(
    ClientInterface $client,
    Checksum $checksum,
    LoggerInterface $logger,
    TimeInterface $time
  ): OtpSimplePayClientInterface {
    return new OtpSimplePayClient(
      $client,
      $checksum,
      $logger,
      (new \DateTime())->setTimestamp($time->getCurrentTime())
    );
  }

}
