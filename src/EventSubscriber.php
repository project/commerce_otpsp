<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp;

use Cheppers\OtpspClient\DataType\Address;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_otpsp\Event\PaymentRequestEvent;
use Drupal\commerce_price\Calculator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EventSubscriber implements EventSubscriberInterface, ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PaymentRequestBuilderInterface::EVENT_ALTER => [
        ['onCommerceOtpspPaymentRequestAlter', 50],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function onCommerceOtpspPaymentRequestAlter(PaymentRequestEvent $event) {
    $paymentConfig = $event
      ->payment
      ->getPaymentGateway()
      ->getPluginConfiguration();

    if (empty($paymentConfig['send_shipping'])
      || !$this->entityTypeManager->hasDefinition('commerce_shipment')
    ) {
      return $this;
    }

    $order = $event->payment->getOrder();

    $event->paymentRequest->shippingCost = intval($this->getShippingCost($order));

    $shippingAddressData = $this->getShippingAddressData($order);
    if ($shippingAddressData) {
      $event->paymentRequest->delivery = Address::__set_state($shippingAddressData);
    }

    return $this;
  }

  protected function getShippingCost(OrderInterface $order): string {
    $shippingCost = '0';

    /** @var \Drupal\commerce_order\Adjustment[] $shipmentAdjustments */
    $shipmentAdjustments = $order->getAdjustments(['shipping']);
    $this->calculateShipmentPrices($shipmentAdjustments, $shippingCost);

    return $shippingCost;
  }

  /**
   * Iterates over given array of any kind of shipments: standalone Shipment entities or Adjustments of the order.
   *
   * @param array $shipments
   * @param $shippingCost
   *
   * @return string
   */
  protected function calculateShipmentPrices(array $shipments, string &$shippingCost): string {
    foreach ($shipments as $shipment) {
      $price = $shipment->getAmount();
      if (!$price) {
        continue;
      }
      $shippingCost = Calculator::add($shippingCost, $price->getNumber());
    }

    return $shippingCost;
  }

  protected function getShippingAddressData(OrderInterface $order): array {
    /** @var \Drupal\commerce_shipping\ShipmentStorageInterface $shipmentStorage */
    $shipmentStorage = $this->entityTypeManager->getStorage('commerce_shipment');
    $shipments = $shipmentStorage->loadMultipleByOrder($order);
    $shipment = reset($shipments);
    if (!$shipment) {
      return [];
    }

    $profile = $shipment->getShippingProfile();
    if (!$profile->hasField('address')) {
      return [];
    }

    $address = $profile->get('address');
    if ($address->isEmpty()) {
      return [];
    }

    return Utils::transformAddressField($address->first()->getValue());
  }

}
