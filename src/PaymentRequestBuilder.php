<?php

declare(strict_types = 1);

namespace Drupal\commerce_otpsp;

use Cheppers\OtpspClient\Checksum;
use Cheppers\OtpspClient\DataType\Address;
use Cheppers\OtpspClient\DataType\Item;
use Cheppers\OtpspClient\DataType\PaymentRequest;
use Cheppers\OtpspClient\OtpSimplePayClient;
use Cheppers\OtpspClient\OtpSimplePayClientInterface;
use DateTime;
use DateTimeInterface;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_otpsp\Event\PaymentRequestEvent;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PaymentRequestBuilder implements PaymentRequestBuilderInterface, ContainerInjectionInterface {

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * @var array
   */
  protected $paymentConfig;

  /**
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $store;

  /**
   * @var \Cheppers\OtpspClient\OtpSimplePayClient
   */
  protected $client;

  /**
   * @var \Cheppers\OtpspClient\Checksum
   */
  protected $checksum;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * @var \Cheppers\OtpspClient\DataType\PaymentRequest
   */
  protected $paymentRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('date.formatter'),
      $container->get('commerce_otpsp.client'),
      $container->get('commerce_otpsp.checksum'),
      $container->get('commerce_otpsp.random')
    );
  }

  public function __construct(
    EventDispatcherInterface $eventDispatcher,
    TimeInterface $time,
    AccountProxyInterface $currentUser,
    LanguageManagerInterface $languageManager,
    DateFormatter $dateFormatter,
    OtpSimplePayClient $client,
    Checksum $checksum,
    Random $random
  ) {
    $this->client = $client;
    $this->checksum = $checksum;
    $this->time = $time;
    $this->currentUser = $currentUser;
    $this->languageManager = $languageManager;
    $this->dateFormatter = $dateFormatter;
    $this->eventDispatcher = $eventDispatcher;
    $this->random = $random;

    $this->paymentRequest = new PaymentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentRequest(PaymentInterface $payment): PaymentRequest {
    $this->payment = $payment;

    return $this
      ->init()
      ->applyBasic()
      ->applyItems()
      ->alter()
      ->paymentRequest;
  }

  /**
   * @return $this
   */
  protected function init() {
    $this->paymentRequest = new PaymentRequest();
    // @todo Validate whether the payment does belong to an order.
    $this->order = $this->payment->getOrder();

    $this->paymentConfig = $this
      ->payment
      ->getPaymentGateway()
      ->getPluginConfiguration();

    $this
      ->client
      ->setSecretKey($this->paymentConfig['secret_key'])
      ->setBaseUriByMode($this->paymentConfig['mode']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function applyBasic() {
    $routeParameters = [
      'commerce_order' => $this->order->id(),
      // @todo Get the first step.
      'step' => 'payment',
    ];
    $routeOptions = ['absolute' => TRUE];

    $billingData = $this->getBillingData();

    $this->paymentRequest->merchant = $this->paymentConfig['merchant_id'];
    $this->paymentRequest->orderRef = $this->payment->uuid();
    $this->paymentRequest->customer = $billingData['name'];
    $this->paymentRequest->customerEmail = $this->order->getEmail();
    $this->paymentRequest->language = $this->getLangCode();
    $this->paymentRequest->currency = $this->payment->getAmount()->getCurrencyCode();
    $this->paymentRequest->total = intval($this->payment->getAmount()->getNumber());
    $this->paymentRequest->discount = $this->calculateOrderDiscount();

    $this->paymentRequest->salt = $this->random->string(32);
    $this->paymentRequest->invoice = Address::__set_state($billingData);
    $this->paymentRequest->timeout = $this->getTimeout()->format(OtpSimplePayClientInterface::DATETIME_FORMAT);
    $this->paymentRequest->urls->success = Url::fromRoute(
      'commerce_payment.checkout.return',
      $routeParameters,
      $routeOptions
    )->toString();
    $this->paymentRequest->urls->fail = Url::fromRoute(
      'commerce_payment.checkout.cancel',
      $routeParameters,
      $routeOptions
    )->toString();
    $this->paymentRequest->urls->cancel = $this->paymentRequest->urls->fail;
    $this->paymentRequest->urls->timeout = $this->paymentRequest->urls->fail;

    return $this;
  }

  public function calculateOrderDiscount() {
    $amount = new Price('0', $this->order->getTotalPrice()->getCurrencyCode());
    $order_total_summary = \Drupal::service('commerce_order.order_total_summary');
    $summary = $order_total_summary->buildTotals($this->order);

    foreach ($summary['adjustments'] as $adjustment) {
      if (in_array($adjustment['type'], ['tax', 'shipping']) || $adjustment['included'] === TRUE) {
        continue;
      }
      $amount = $amount->add($adjustment['amount']);
    }

    return \Drupal::getContainer()->get('commerce_price.rounder')->round($amount)->getNumber();
  }

  /**
   * @return $this
   */
  protected function applyItems() {
    foreach ($this->order->getItems() as $orderItem) {
      $item = new Item();
      $item->title = $orderItem->getTitle();
      $item->price = intval($orderItem->getAdjustedUnitPrice(['tax'])->getNumber());
      $item->amount = intval($orderItem->getQuantity());
      $item->tax = 0;

      if ($orderItem->hasPurchasedEntity()) {
        $item->ref = $this->getPurchasedEntityIdentifier($orderItem->getPurchasedEntity());
      }

      $this->paymentRequest->items[$orderItem->id()] = $item;

    }

    return $this;
  }

  protected function alter() {
    $event = new PaymentRequestEvent();
    $event->payment = $this->payment;
    $event->paymentRequest = $this->paymentRequest;
    $this->eventDispatcher->dispatch(static::EVENT_ALTER, $event);

    return $this;
  }

  protected function getBillingData(): array {
    $billingProfile = $this->order->getBillingProfile();
    if (!$billingProfile->hasField('address')) {
      return [];
    }

    $address = $billingProfile->get('address');
    if ($address->isEmpty()) {
      return [];
    }

    return Utils::transformAddressField($address->first()->getValue());
  }

  protected function getTimeout(): DateTimeInterface {
    return DateTime::createFromFormat('U', (string) $this->time->getRequestTime())
      ->modify("+{$this->paymentConfig['timeout']} seconds");
  }

  protected function getLangCode(): string {
    $supportedLangCodes = $this->client->getSupportedLanguages();

    $langCode = $this->languageManager->getCurrentLanguage()->getId();
    if (in_array($langCode, $supportedLangCodes)) {
      return $langCode;
    }

    $langCode = $this->currentUser->getPreferredLangcode(FALSE);
    if ($langCode && in_array($langCode, $supportedLangCodes)) {
      return $langCode;
    }

    $langCode = $this->languageManager->getDefaultLanguage()->getId();

    return in_array($langCode, $supportedLangCodes) ? $langCode : 'hu';
  }

  protected function getDiscountAmountFromOrderItem(OrderItemInterface $orderItem): string {
    $discount = '0';
    foreach ($orderItem->getAdjustments(['promotion']) as $adjustment) {
      $price = $adjustment->getAmount();
      if (!$price) {
        continue;
      }

      $discount = Calculator::add($discount, $price->getNumber());
    }

    return $discount;
  }

  protected function getPurchasedEntityIdentifier(PurchasableEntityInterface $product): string {
    $fieldNames = ['sku', $product->getEntityType()->getKey('uuid')];
    foreach ($fieldNames as $fieldName) {
      if (!$product->hasField($fieldName)) {
        continue;
      }

      $values = $product->get($fieldName);
      if ($values->isEmpty()) {
        continue;
      }

      return $values->first()->getString();
    }

    return $product->id();
  }

}
