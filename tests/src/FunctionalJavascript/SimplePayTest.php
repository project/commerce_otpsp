<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_otpsp\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Tests the SimplePay.
 *
 * @group commerce_otpsp
 */
class SimplePayTest extends CommerceWebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_cart',
    'commerce_product',
    'commerce_order',
    'commerce_payment',
    'commerce_store',
    'commerce_price',
    'commerce_checkout',
    'commerce_shipping',
    'commerce_otpsp',
  ];

  /**
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $productVariation = NULL;

  /**
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product = NULL;

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway = NULL;

  /**
   * @var string
   */
  protected $cardNumberSuccessVisa = '4908366099900425';

  /**
   * @var string
   */
  protected $cardNumberSuccessMasterCard = '5587402000012011';

  /**
   * @var string
   */
  protected $cardNumberFailed = '4111111111111111';

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $account = $this->drupalCreateUser();
    $account
      ->setEmail(mb_strtolower($account->getEmail()))
      ->save();
    $this->drupalLogin($account);

    /** @var \Drupal\commerce_price\Entity\Currency $currency */
    $currency = $this->createEntity('commerce_currency', [
      'currencyCode' => 'HUF',
      'name' => 'Hungarian forint',
      'numericCode' => 348,
      'symbol' => 'HUF',
      'fractionDigits' => 2,
    ]);

    $this->store->setDefaultCurrency($currency);

    /** @var \Drupal\commerce_shipping\Entity\PackageType $packageType */
    $packageType = $this->createEntity('commerce_package_type', [
      'id' => 'package_type_a',
      'label' => 'Package Type A',
      'dimensions' => [
        'length' => 20,
        'width' => 20,
        'height' => 20,
        'unit' => 'mm',

      ],
      'weight' => [
        'number' => 20,
        'unit' => 'g',
      ],
    ]);
    $this->container->get('plugin.manager.commerce_package_type')->clearCachedDefinitions();

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Overnight shipping',
      'stores' => [$this->store->id()],
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'default_package_type' => 'commerce_package_type:' . $packageType->get('uuid'),
          'rate_label' => 'Overnight shipping',
          'rate_amount' => [
            'number' => '100',
            'currency_code' => 'HUF',
          ],
        ],
      ],
    ]);

    $this->paymentGateway = PaymentGateway::create([
      'id' => 'simplepay',
      'label' => 'SimplePay',
      'plugin' => 'commerce_otpsp_redirect',
      'configuration' => [
        'display_label' => 'SimplePay',
        'merchant_id' => 'PUBLICTESTHUF',
        'secret_key' => 'FxDa5w314kLlNseq2sKuVwaqZshZT5d6',
        'send_shipping' => TRUE,
      ],
    ]);
    $this->paymentGateway->save();

    $this->productVariation = $this->createEntity(
      'commerce_product_variation',
      [
        'type' => 'default',
        'sku' => '0001',
        'price' => [
          'number' => '20',
          'currency_code' => 'HUF',
        ],
        'weight' => [
          'number' => '1',
          'unit' => 'kg',
        ],
      ]
    );

    $this->product = $this->createEntity(
      'commerce_product',
      [
        'type' => 'default',
        'title' => 'my product 01',
        'id' => 'my product 01',
        'stores' => [$this->store],
        'variations' => [$this->productVariation],
      ]
    );
  }

  protected function setUpShipping() {
    /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $productVariationType */
    $productVariationType = $this
      ->entityTypeManager
      ->getStorage('commerce_product_variation_type')
      ->load('default');
    $productVariationType
      ->setTraits(['purchasable_entity_shippable'])
      ->save();

    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $orderType */
    $orderType = $this
      ->entityTypeManager
      ->getStorage('commerce_order_type')
      ->load('default');
    $orderType
      ->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'shipping')
      ->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default')
      ->save();

    $fieldDefinition = commerce_shipping_build_shipment_field_definition($orderType->id());
    $this
      ->container
      ->get('commerce.configurable_field_manager')
      ->createField($fieldDefinition);

    $traitManager = $this->container->get('plugin.manager.commerce_entity_trait');
    /** @var \Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitInterface $trait */
    $trait = $traitManager->createInstance('purchasable_entity_shippable');
    $traitManager->installTrait($trait, 'commerce_product_variation', 'default');

    drupal_flush_all_caches();
  }

  public function testCancelAndSuccess() {
    $this->drupalGet($this->product->toUrl()->toString());
    $this->submitForm([], 'Add to cart');
    $this->drupalGet('checkout/1');

    $billingAddressPrefix = 'payment_information[billing_information][address][0][address]';
    $this->submitForm(
      [
        "{$billingAddressPrefix}[given_name]" => 'Johnny',
        "{$billingAddressPrefix}[family_name]" => 'Appleseed',
        "{$billingAddressPrefix}[address_line1]" => '123 New York Drive',
        "{$billingAddressPrefix}[locality]" => 'New York City',
        "{$billingAddressPrefix}[administrative_area]" => 'NY',
        "{$billingAddressPrefix}[postal_code]" => '10001',
      ],
      'Continue to review'
    );

    // Click on "back to merchant".
    $this->getSession()->wait(4000);
    $this->submitForm([], 'Pay and complete purchase');
    $this->getSession()->wait(4000);
    $this->getSession()->getPage()->clickLink('actionCancel');
    $this->getSession()->wait(4000);
    $this->assertSession()->pageTextContains('You have canceled checkout at SimplePay but may resume the checkout process here when you are ready.');

    // Wrong card.
    $this->submitForm([], 'Pay and complete purchase');
    $card = $this->getSession()->getPage()->findById('cardNoSelect');
    $card->setValue($this->cardNumberFailed);
    $this->getSession()->getPage()->pressButton('actionPay');
    $this->getSession()->wait(4000);
    $this->assertSession()->pageTextContains('Something went wrong with the given credit card');

    // Success.
    $this->submitForm([], 'Pay and complete purchase');
    $actualSimplePayAddresses = $this->getActualSimplePayAddresses();
    $this->assertArrayHasKey('Billing address', $actualSimplePayAddresses);
    $this->assertEqual(
      $actualSimplePayAddresses['Billing address'],
      [
        'invoice_name' => 'Appleseed Johnny',
        'invoice_email' => $this->loggedInUser->getEmail(),
        'invoice_city' => '10001 / New York City',
        'invoice_address' => '123 New York Drive',
        'invoice_country' => 'US',
        'invoice_phone' => '',
      ]
    );
    $this
      ->getSession()
      ->getPage()
      ->findById('cardNoSelect')
      ->setValue($this->cardNumberSuccessVisa);
    $this->getSession()->getPage()->pressButton('actionPay');
    $this->getSession()->wait(4000);
    $this->assertSession()->pageTextContains('Your order number is 1.');

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load(1);

    /** @var \Drupal\commerce_payment\PaymentStorageInterface $paymentStorage */
    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');

    $payments = $paymentStorage->loadMultipleByOrder($order);
    $this->assertEqual(count($payments), 3, 'Order has three payments');

    $canceledPayment = array_shift($payments);
    $this->assertSame('authorization_voided', $canceledPayment->getState()->getString(), 'Canceled payment state');
    $this->assertSame('CANCEL', $canceledPayment->getRemoteState(), 'Canceled payment remote state');
    $this->assertNotEmpty($canceledPayment->getRemoteId(), 'Canceled payment remote ID');

    $canceledPayment = array_shift($payments);
    $this->assertSame('authorization_voided', $canceledPayment->getState()->getString(), 'Wrong payment state');
    $this->assertSame('FAIL', $canceledPayment->getRemoteState(), 'Wrong payment remote state');
    $this->assertNotEmpty($canceledPayment->getRemoteId(), 'Wrong payment remote ID');

    $successPayment = array_shift($payments);
    $this->assertSame('authorization', $successPayment->getState()->getString(), 'Success payment state');
    $this->assertSame('INFRAUD', $successPayment->getRemoteState(), 'Success payment remote state');
    $this->assertNotEmpty($successPayment->getRemoteId(), 'Success payment remote ID');
  }

  public function testDataSend() {
    $this->setUpShipping();

    $orderId = 1;

    $this->drupalGet($this->product->toUrl()->toString());
    $this->submitForm([], 'Add to cart');

    $this->drupalGet('cart');
    $this->submitForm([], 'Checkout');

    $billingAddress = [
      'given_name' => 'John',
      'family_name' => 'Smith',
      'address_line1' => '1098 Alta Ave',
      'locality' => 'Mountain View',
      'administrative_area' => 'CA',
      'postal_code' => '94041',
    ];
    $shippingAddress = [
      'given_name' => 'John2',
      'family_name' => 'Smith2',
      'address_line1' => '1098 Alta Ave',
      'locality' => 'Mountain View2',
      'administrative_area' => 'CA',
      'postal_code' => '94042',
    ];

    $this->getSession()->wait(4000);
    $this
      ->fillAddressField(
        'payment_information[billing_information][address][0][address]',
        $billingAddress
      )
      ->fillAddressField(
        'shipping_information[shipping_profile][address][0][address]',
        $shippingAddress
      );

    $page = $this->getSession()->getPage();
    $page->findButton('Recalculate shipping')->click();
    $this->waitForAjaxToFinish();
    $page->pressButton('Continue to review');
    $this->getSession()->wait(4000);
    $this->submitForm([], 'Pay and complete purchase');

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($orderId);
    $orderTotalPrice = $order->getTotalPrice();

    /** @var \Drupal\commerce_shipping\ShipmentStorageInterface $shipmentStorage */
    $shipmentStorage = $this->entityTypeManager->getStorage('commerce_shipment');
    $shipmentPrice = '0';
    foreach ($shipmentStorage->loadMultipleByOrder($order) as $shipment) {
      $shipmentPrice = Calculator::add($shipmentPrice, $shipment->getAmount()->getNumber());
    }

    $this->getSession()->wait(4000);
    $simplePayTotalPrice = $this->getSimplePayTotalPrice();
    $this->assertSame(
      (float) $orderTotalPrice->getNumber(),
      (float) $simplePayTotalPrice->getNumber(),
      'order total price number'
    );
    $this->assertSame(
      $orderTotalPrice->getCurrencyCode(),
      $simplePayTotalPrice->getCurrencyCode(),
      'order total price currency'
    );

    // @todo Assert product list.
    $actualAddresses = $this->getActualSimplePayAddresses();

    $this->assertArrayHasKey('Billing address', $actualAddresses);
    $this->assertEqual(
      $actualAddresses['Billing address'],
      [
        'invoice_name' => "{$billingAddress['family_name']} {$billingAddress['given_name']}",
        'invoice_email' => $order->getEmail(),
        'invoice_city' => "{$billingAddress['postal_code']} / {$billingAddress['locality']}",
        'invoice_address' => $billingAddress['address_line1'],
        'invoice_country' => 'US',
        'invoice_phone' => '',
      ],
      'billing address'
    );

    $this->assertArrayHasKey('Shipping address', $actualAddresses);
    $this->assertEqual(
      $actualAddresses['Shipping address'],
      [
        'delivery_name' => "{$shippingAddress['family_name']} {$shippingAddress['given_name']}",
        'delivery_city' => "{$shippingAddress['postal_code']} / {$shippingAddress['locality']}",
        'delivery_address' => $shippingAddress['address_line1'],
        'delivery_country' => 'US',
        'delivery_phone' => '',
      ],
      'shipping address'
    );

    $this->assertSame(
      [
        [
          'title' => 'my product 01',
          'amount' => '1',
          'price_number' => '20',
          'price_currency' => 'HUF',
        ],
        [
          'title' => 'Shipping cost',
          'amount' => '',
          // There is a bug on the SimplePay's side.
          'price_number' => '',
          'price_currency' => 'HUF',
        ],
      ],
      $this->getSimplePayItems(),
      'products'
    );
  }

  /**
   * @return $this
   */
  protected function fillAddressField(string $prefix, array $values) {
    // @todo Fill the country first and wait for the AJAX to finish.
    $page = $this->getSession()->getPage();
    foreach ($values as $property => $value) {
      $page->fillField("{$prefix}[{$property}]", $value);
    }

    return $this;
  }

  protected function getActualSimplePayAddresses(): array {
    $wrappers = $this
      ->getSession()
      ->getPage()
      ->findAll('css', '.addresslist-item');

    $addresses = [];
    foreach ($wrappers as $wrapper) {
      $address = $this->parseSimplePayAddressWrapper($wrapper);
      $addresses[$address['title']] = $address['values'];
    }

    return $addresses;
  }

  protected function parseSimplePayAddressWrapper(NodeElement $wrapper): array {
    $address = [
      'title' => '',
      'values' => [],
    ];
    $titleElement = $wrapper->find('css', '.title');
    $address['title'] = trim($titleElement->getHtml(), "\n\r\t :");

    $elementLocators = [
      'invoice_name' => '#invoice_name',
      'invoice_email' => '#invoice_email',
      'invoice_city' => '#invoice_city',
      'invoice_address' => '#invoice_address',
      'invoice_country' => '#invoice_country',
      'invoice_phone' => '#invoice_phone',
      'delivery_name' => '#delivery_name',
      'delivery_city' => '#delivery_city',
      'delivery_address' => '#delivery_address',
      'delivery_country' => '#delivery_country',
      'delivery_phone' => '#delivery_phone',
    ];

    foreach ($elementLocators as $name => $locator) {
      $element = $wrapper->find('css', $locator);
      if (!$element) {
        continue;
      }
      $address['values'][$name] = trim($element->getHtml());
    }

    return $address;
  }

  protected function getSimplePayTotalPrice(): Price {
    $wrapper = $this
      ->getSession()
      ->getPage()
      ->find('css', '.sumpc');

    $replacementPairs = [
      '&nbsp;' => '',
      ' ' => '',
    ];
    switch ($this->getSimplePayLangCode()) {
      case 'en':
        $replacementPairs[','] = '';
        break;
    }

    $number = trim($wrapper->find('css', '.value')->getText());
    $number = strtr($number, $replacementPairs);
    $currency = $wrapper->find('css', '.currency')->getText();

    return new Price($number, $currency);
  }

  protected function getSimplePayLangCode(): string {
    // CSS #language-chooser.
    return 'en';
  }

  protected function getSimplePayItems(): array {
    $items = [];

    $rowElements = $this
      ->getSession()
      ->getPage()
      ->findAll('css', '#productlist tbody tr');

    $columnNames = [
      'title',
      'amount',
      'price',
    ];

    foreach ($rowElements as $rowIndex => $rowElement) {
      $items[$rowIndex] = [
        'title' => '',
        'amount' => '',
        'price_number' => '',
        'price_currency' => '',
      ];

      foreach ($rowElement->findAll('css', 'td') as $columnIndex => $cellElement) {
        $columnName = $columnNames[$columnIndex];
        $content = trim(strtr($cellElement->getHtml(), ['&nbsp;' => '']));
        switch ($columnName) {
          case 'title':
          case 'amount':
            $items[$rowIndex][$columnName] = $content;
            break;

          case 'price':
            $parts = preg_split('/\s+/', $content);
            if (count($parts) === 1) {
              array_unshift($parts, '');
            }

            $items[$rowIndex]['price_number'] = $parts[0];
            $items[$rowIndex]['price_currency'] = $parts[1];
            break;
        }
      }
    }

    return $items;
  }

}
